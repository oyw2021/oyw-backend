from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from typing import Text
import env
import os


MYSQL_URL = env.MYSQL_URL

engine = create_engine(MYSQL_URL, echo = False)

SessionLocal = sessionmaker(
    autocommit = False, 
    autoflush = False, 
    bind = engine
)

Base = declarative_base()