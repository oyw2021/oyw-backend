
import firebase_admin
from firebase_admin import credentials
import env
from firebase_admin import db

cred = credentials.Certificate("firebase_auth.json")
x = firebase_admin.initialize_app(cred, {
    'databaseURL': env.FIREBASE_DB_URL
})

ref = db.reference('/')


