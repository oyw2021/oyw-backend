class StandardResponseBody:

    def __init__(self, result : bool, message : str, token = None, data = None):
        # result successful ? True : False
        self.result, self.message = result, message

        if token:
            self.token = token
        
        if data:
            self.data =  data