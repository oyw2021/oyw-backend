from os import stat_result
from fastapi import APIRouter, Request, BackgroundTasks, Depends
from responses.standard_response_body import StandardResponseBody
from tokens import controllers as token_controllers
from admins import controllers as admin_controllers
from users import controllers as user_controllers
from passes import controllers as pass_controllers
from admins import schemas as admin_schemas
from users import schemas as user_schemas
from users.roles import RoleChecker
from db_setup import firebase_setup
from pydantic import Field



check_if_admin = RoleChecker("SA")
check_if_head = RoleChecker(["SH", "RH", "SA"])

router = APIRouter()


USER_MAP = {
    "SA": ["SA", "SH", "RH", "SU", "RU"],
    "SH": ["SU"],
    "RH": ["RU"],
    "SU": [], "RU": []
}


@router.post("/view_users/", summary = "Retrieves users")
async def view_users(token: dict = Depends(token_controllers.token_validation)):
    if not check_if_admin(token.role):
        return StandardResponseBody(False, "You are not authorized to view users.", token = token.token_value)
    
    res = await admin_controllers.view_users(token.role)
    if res:
        # print(res)
        return StandardResponseBody(
            True, "User details", token = token.token_value, data = res
        )
    return StandardResponseBody(
        False, "Cannot retrieve user details", token = token.token_value
    )
    

@router.post("/register_user/", summary = "Register users")
async def register_user(user: user_schemas.UserSignUp, token : dict = Depends(token_controllers.token_validation)):
    #print(request.base_url)
    if user.role not in USER_MAP[token.role]:
        return StandardResponseBody(
            False, "You are not authorized to register this type of user.", token = token.token_value
        )
        
    res = await user_controllers.register_user(user)
    if res == True:
        return StandardResponseBody(
            True, "Your account has been created", token = token.token_value
        )
    elif res == "exists":
        return StandardResponseBody(
            False, "An account with that username already exists", token = token.token_value
        )
    return StandardResponseBody(
        False, "Account not created", token = token.token_value
    )


@router.post("/enable_user/", summary = "Enables user to perform operations")
async def enable_user(user: user_schemas.UserSchema, token: dict = Depends(token_controllers.token_validation)):
    if not check_if_head(token.role):
        return StandardResponseBody(
            False, "You are not authorized to manage users.", token = token.token_value
        )
    
    res = await admin_controllers.enable_user(user = user, role = token.role)
    if res:
        # print(res)
        return StandardResponseBody(
            True, "User enabled", token = token.token_value, data = res
        )
    return StandardResponseBody(
        False, "Cannot enable user.", token = token.token_value
    )


@router.post("/disable_user/", summary = "Disables user performing operations")
async def disable_user(user: user_schemas.UserSchema, token: dict = Depends(token_controllers.token_validation)):
    if not check_if_head(token.role):
        return StandardResponseBody(False, "You are not authorized to manage users.", token = token.token_value)
    
    res = await admin_controllers.disable_user(user = user, role = token.role)
    if res:
        # print(res)
        return StandardResponseBody(
            True, "User disabled", token = token.token_value, data = res
        )
    return StandardResponseBody(
        False, "Cannot disable user.", token = token.token_value
    )


@router.post("/view_transactions/", summary = "For viewing registration transactions")
async def registration_transactions(token: dict = Depends(token_controllers.token_validation)):
    transactions = await admin_controllers.get_transactions()
    
    if transactions is not None:
        return StandardResponseBody(
                True, "Transactions retrieved", token = token.token_value, data = transactions
            )
    else:
        return StandardResponseBody(
                False, "Could not retrieve transactions", token = token.token_value
            )


@router.post("/undo_register/", summary = "Undo pass registration")
async def undo_register(transaction: admin_schemas.TransactionSchema, token: dict = Depends(token_controllers.token_validation)):
    if not check_if_head(token.role):
        return StandardResponseBody(False, "You are not authorized to unregister passes.", token = token.token_value)
    else:
        del_result = await admin_controllers.undo_register_controller(transaction.transaction_id)
        await pass_controllers.update_total_pass_counts()
        if del_result == "balance":
            return StandardResponseBody(False, "Pass does not have sufficient balance", token = token.token_value, data = del_result)
        elif del_result:
            return StandardResponseBody(True, "Pass has been unregistered", token = token.token_value, data = del_result)
    return StandardResponseBody(False, "Transaction does not exist", token = token.token_value, data = del_result)


@router.post("/undo_scan/", summary = "Undo pass scan")
async def undo_scan(transaction:  admin_schemas.TransactionSchema, token: dict = Depends(token_controllers.token_validation)):
    if not check_if_head(token.role):
        return StandardResponseBody(False, "You are not authorized to unregister scans.", token = token.token_value)
    else:
        del_result = await admin_controllers.undo_scan_controller(transaction.transaction_id)
        await pass_controllers.update_total_pass_counts()
        if del_result == "balance":
            return StandardResponseBody(False, "Pass does not have sufficient balance", token = token.token_value, data = del_result)
        elif del_result:
            return StandardResponseBody(True, "Pass scan undo successful", token = token.token_value, data = del_result)
    return StandardResponseBody(False, "Transaction does not exist", token = token.token_value, data = del_result)
