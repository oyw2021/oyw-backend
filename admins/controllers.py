from passes import controllers as pass_controllers
from users import models as user_models
from passes import models as pass_models
from passes import firebase_model
import settings


ROLE_MAP = {
    "SA": ["SA", "RH", "SH", "RU", "SU"],
    "RH": ["RH", "RU"],
    "SH": ["SH", "SU"]
}

async def view_users(role):
    users = await user_models.view_users(ROLE_MAP[role])
    users = [
        user.__dict__ for user in users
    ]
    return users


async def enable_user(user, role):
    ### get user object
    db_user = await user_models.get_user_by_phone(user.phone)
    if not db_user:
        return False
    
    if db_user.role in ROLE_MAP[role]:
        res = await user_models.enable_user(db_user)
        if res:
            return True
    return False


async def disable_user(user, role):
    ### get user object
    db_user = await user_models.get_user_by_phone(user.phone)
    if not db_user:
        return False
    
    if db_user.role in ROLE_MAP[role]:
        res = await user_models.disable_user(db_user)
        if res:
            return True
    return False


async def get_transactions():
    registration_transactions = await firebase_model.get_registration_transactions()
    scan_transactions = await firebase_model.get_scan_transactions()
    transactions = {
        'registrations' : [],
        'scans': []
    }
    if registration_transactions is  not None:
        for t in registration_transactions:
            registration_transactions[t]["id"] = t
        transactions['registrations'] = [registration_transactions[i] for i in registration_transactions]
    if scan_transactions is not None:
        for t in scan_transactions:
            scan_transactions[t]["id"] = t
        transactions['scans'] = [scan_transactions[i] for i in scan_transactions]
    return transactions


async def undo_register_controller(t_id : str):   
    ### Get db pass object based on phone or email
    transaction = await firebase_model.get_registration_transaction_by_id(t_id)
    if transaction:
        
        ### undo from db
        db_pass = await pass_models.get_pass_by_code(transaction["code"])
        if db_pass:
                    
            ### update values in db
            res = await pass_controllers.undo_pass_register(db_pass, transaction)
            # print(res)
            
            if res == "balance": return res
            
            elif res:    
                status = await firebase_model.delete_registration_transaction(t_id)
                print(status)
                return status
    return False


async def undo_scan_controller(t_id : str):   
    ### Get db pass object based on phone or email
    transaction = await firebase_model.get_scan_transaction_by_id(t_id)
    if transaction:
        
        ### undo from db
        db_pass = await pass_models.get_pass_by_code(transaction["code"])
        if db_pass: 
            ### update values in db
            res = await pass_controllers.undo_pass_scan(db_pass, transaction)
            # print(res)
            
            if res == "balance": return res
            
            elif res:    
                status = await firebase_model.delete_scan_transaction(t_id)
                # print(status)
                return status
    return False