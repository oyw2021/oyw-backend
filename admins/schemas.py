from pydantic import BaseModel, Field, validator
from typing import Optional
import settings
import re


class TransactionSchema(BaseModel):
    transaction_id : str