from __future__ import print_function
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from email.mime.multipart import MIMEMultipart
from googleapiclient.discovery import build
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
import base64
import email.encoders as encoder
from apiclient import errors
from passes import models as pass_models
import mimetypes
import os.path
import base64
import pickle
from pathlib import Path
import qrcode
import os
import re
import env
from db_setup.mysql_setup import Base, SessionLocal
import time

    
# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.send']


def authenticate():
    creds = None
    print(os.path.exists('token.pickle'))
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)
    return service


service = authenticate()


def generate_image(code):
    path = f"{str(Path(env.QR_PATH).resolve())}"
    
    qr = qrcode.QRCode(
        version = 2,
        box_size = 6,
        border = 2
    )
    
    qr.add_data(code)
    qr.make(fit = True)
    
    img = qr.make_image()
    img.save(f"{path}/{code}.png")



def create_pass_details_message(db_pass, link):
    name = db_pass.name
    if name == "":
        name = "there"
        
    singing = db_pass.singing_count - db_pass.singing_redeemed_count
    drama = db_pass.drama_count - db_pass.drama_redeemed_count
    dance = db_pass.dance_count - db_pass.dance_redeemed_count
    
    message = MIMEMultipart()
    message['to'] = db_pass.email
    message['from'] = env.MAIL_FROM
    message['subject'] = "Orlem Youth Week - Pass Details"

    message_alternative = MIMEMultipart('alternative')
    message_alternative.attach(MIMEText(f"""Hi {name}!

We're sending you your pass details for Orlem Youth Week 2021.

If you're searching for clues,
Get ready to be amused,
'Coz it's time to infuse,
Your rhythm with some blues!

Today's Event: SINGING
Starts at: 07:30 p.m.

Here are your current pass details:
- Day 1 - Singing: {db_pass.singing_count - db_pass.singing_redeemed_count}
- Day 2 - Drama: {db_pass.drama_count - db_pass.drama_redeemed_count}
- Day 3 - Dance: {db_pass.dance_count - db_pass.dance_redeemed_count}

Please show your QR Code while entering the event.
    Or use the short code: {db_pass.short_code}
All details regarding your ticket(s) are available on the link: {link}

Guidelines for OYW:

- Each QR Pass is issued to one person only (on one phone number / email).
- A single person may purchase passes for several people on the same QR (unique) and have the count decremented on scanning upon entering the venue.
- Children and senior citizens are advised to watch the live stream from the comfort of their homes for their own safety.
- Kindly wear your masks at all times. Attending the event without masks, will not be permitted.
- Avoid crowding or forming groups at any place during the event.
- Please follow the COVID-19 guidelines for your own safety and safety of the others attending the event.
- Anyone found breaching the Covid-19 guidelines will be escorted out.
- The church is not responsible for the health and safety of anyone attending the event.
By buying your QR Pass you agree to all the rules and regulations mentioned above and accept to follow them.

In case you have any pass related issues, contact us at devs@orlemyouthweek.site or at the OYW Registration desk.
Thank you.

Regards,
The OYW Team.
"""))
    message.attach(message_alternative)
    
    body = f"""
    <html>
        <body style="font-family: Verdana, Geneva, Tahoma, sans-serif;">
            <p>Hi {name}! <br>
            We're sending you your pass details for <big>Orlem Youth Week 2021</big>.
            </p>
            <hr>
            <p style="font-style: italic; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; color: #093774;">
                If you're searching for clues,<br>
                Get ready to be amused,<br>
                'Coz it's time to infuse,<br>
                Your rhythm with some blues!<br>
            </p>
            <h3 style="font-family: 'Courier New', Courier, monospace;">
                Today's Event: SINGING<br>
                Starts at: 07:30 p.m.
            </h3>
            <hr>
            <p style="font-size: larger;">
                <span style="text-decoration: underline;">
                    Here are your current pass details:<br>
                </span>
                <ul style="font-size: larger;">
                    <li>Day 1 - Singing: {db_pass.singing_count - db_pass.singing_redeemed_count}</li>
                    <li>Day 2 - Drama: {db_pass.drama_count - db_pass.drama_redeemed_count} </li>
                    <li>Day 3 - Dance: {db_pass.dance_count - db_pass.dance_redeemed_count} </li>
                </ul>
            </p>
            <b>Please show your QR Code while entering the event.<br>
                Or use the short code: <big style="color: #ff0000;">{db_pass.short_code}</big></b>
            <p>All details regarding your ticket(s) are available on the link <b><a href="{link}">{link}</a></b></p>
            <br>
            <a href="{link}" style="
                    background-color: #34b68b; border: none;
                    color: white;
                    padding: 10px;
                    border-radius: 5px;
                    text-align: center;
                    text-decoration: none;
                    transition: 500;
                    display: inline-block;
                    font-size: 16px;"
                    onMouseOver="this.style.backgroundColor='#000000';"
                    onMouseOut="this.style.backgroundColor='#34b68b'"
                    >
                Click here to view pass details
            </a>
        <br><br><hr>
        <p style="font-weight: bold; text-decoration: underline;">Guidelines for OYW</p>
            <ol>
                <li>Each QR Pass is issued to one person only (on one phone number / email).</li> 
                <li>A single person may purchase passes for several people on the same QR (unique) and have the count decremented on scanning upon entering the venue.</li> 
                <li>Children and senior citizens are advised to watch the live stream from the comfort of their homes for their own safety.</li> 
                <li>Kindly wear your masks at all times. Attending the event without masks, will not be permitted.</li> 
                <li>Avoid crowding or forming groups at any place during the event.</li> 
                <li>Please follow the COVID-19 guidelines for your own safety and safety of the others attending the event.</li> 
                <li>Anyone found breaching the Covid-19 guidelines will be escorted out.</li> 
                <li>The church is not responsible for the health and safety of anyone attending the event.</li>
                </ol>
            By buying your QR Pass you agree to all the rules and regulations mentioned above and accept to follow them.
        <p> 
        <br>
        <p>In case you have any pass related issues, contact us at <a href="mailto:devs@orlemyouthweek.site">devs@orlemyouthweek.site</a>, or at the OYW Registration desk.
        <br>Thank you.
        </p>
        <br>
        <br>
        Regards,<br>
        The OYW Team.
        </p>
        </body>
    </html>
        """
    message_body = MIMEText(body, 'html')
    message_alternative.attach(message_body)

    #Attach Image 
    fp = open(f'templates/qrs/{db_pass.code}.png', 'rb') #Read image 
    message_image = MIMEImage(fp.read())
    fp.close()
    
    # Define the image's ID as referenced above
    message_image.add_header("Content-disposition", "attachment", filename=f"qr_code_{db_pass.code}")

    message.attach(message_image)
    
    return {"raw": base64.urlsafe_b64encode(message.as_string().encode()).decode()}



def send_mail(service, user_id, message):
  try:
    message = (service.users().messages().send(userId=user_id, body=message).execute())
    # print('Message Id: %s' % message['id'])
    return message
  except Exception as error:
    print('An error occurred: %s' % error)
    return None


def send_pass_details_mail(db_pass):
    ###generate image
    generate_image(code = db_pass.code)
    
    link = f"{env.PASS_LINK}{db_pass.code}"
    
    message = create_pass_details_message(db_pass, link)
    # print(message)
    
    res = send_mail(service = service, user_id = env.MAIL_FROM, message = message)
    print(res)
    return res


# if __name__ == "__main__":
#     db = SessionLocal()
#     passes = db.query(pass_models.Passes).all()
#     for db_pass in passes:
#         try:
#             x = send_pass_details_mail(db_pass)
#             print(db_pass.email, x)
#         except Exception as e:
#             print(db_pass.email, x, e)
#         time.sleep(1)
        