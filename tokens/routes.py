from responses.standard_response_body import StandardResponseBody
from tokens import controllers as token_controllers
from fastapi.param_functions import Depends
from tokens import routes as token_routes
from fastapi import APIRouter


router = APIRouter()


@router.post("/validate/", summary = "Validate token")
async def validate_token(token: dict = Depends(token_controllers.token_validation)):
    return StandardResponseBody(
        True, "Valid token", token.token_value
    )


@router.post("/refresh/", summary = "Refresh or generate token")
async def refresh_token(token: dict = Depends(token_controllers.token_validation)):
    res = await token_controllers.refresh_token_by_token(token.token_value)
    #print(res)
    if res:
        return StandardResponseBody(
            True, "Token refreshed", res.token_value
        )
    return StandardResponseBody(
        False, "Token not refreshed"
    )