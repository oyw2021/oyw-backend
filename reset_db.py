from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Numeric, BigInteger
from sqlalchemy_utils import database_exists, create_database, drop_database
from db_setup.mysql_setup import SessionLocal, Base, engine
from users import controllers as user_controllers
from db_setup.firebase_setup import ref
from sqlalchemy import create_engine
import datetime
import uvicorn
import bcrypt
import env

db = SessionLocal()

from users.models import User
from tokens.models import Token
from passes.models import Passes


def get_user_by_phone(phone: str):
    return db.query(User).filter(
        User.phone == phone
    ).first()


def hash_password(password):
    return bcrypt.hashpw(
        password.encode("utf-8"), 
        bcrypt.gensalt()
    ).decode("utf-8")


def create_user(user: dict):
    print(user)
    db_user = User(
        **user,
        timestamp=datetime.datetime.now()
    )
    try:
        db.add(db_user)
        db.commit()
        return True
    except Exception as e:
        print(e)
        db.rollback()
        return False


def register_user(user):
    check = get_user_by_phone(user["phone"])
    if check == None:
        user["password"] = hash_password(user["password"])
        
        #print(user, uid)
        res = create_user(user)
        if res:
            return True
        else:
            return False
    return "exists"


if __name__ == "__main__":
     
    ### reset database
    print("WARNING: THIS WILL RESET THE DATABASE.")
    if str(input("Enter DB reset password: ")) == env.DB_RESET_PASSWORD:
        try:
            drop_database(engine.url)
            print("Database dropped.")
        except:
            print("Cannot drop database.")
        try:
            if not database_exists(engine.url):
                create_database(engine.url)
                print("Database created.")
        except:
            print("Cannot create database.")
        try:
            Base.metadata.create_all(bind=engine)
            print("Tables created.")
            
            
            for user in env.ADMINS:
                print(register_user(user))
                
        except Exception as e:
            print("Tables not created.", e)
        
        ###reset firebase
        ref.set({})
        