from fastapi import Depends
from typing import List


class RoleChecker:
    def __init__(self, allowed_roles: List):
        self.allowed_roles = allowed_roles

    def __call__(self, role: str):
        if role not in self.allowed_roles:
            return False
        return True

admin_role_checker = RoleChecker("SA")
register_role_checker = RoleChecker(["RU", "RH", "SA"])
scanner_role_checker = RoleChecker(["SU", "SH", "SA"])
