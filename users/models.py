from sqlalchemy import Column, Integer, String, DateTime, BigInteger, Boolean
from db_setup.mysql_setup import SessionLocal, Base
import datetime

db = SessionLocal()

class User(Base):

    __tablename__ = "user"

    id = Column(BigInteger, primary_key=True, index=True)
    phone = Column(String(10), unique = True)
    password = Column(String(300), nullable=False)
    name = Column(String(30))
    role = Column(String(2))
    enabled = Column(Boolean, default = True)
    timestamp = Column(DateTime)


async def get_user_by_phone(phone: str):
    return db.query(User).filter(
        User.phone == phone
    ).first()


async def get_user_by_id(id: int):
    return db.query(User).filter(
        User.id == id
    ).first()


async def create_user(user: dict):
    db_user = User(
        **user.__dict__,
        timestamp=datetime.datetime.now()
    )
    print(db_user)
    try:
        db.add(db_user)
        db.commit()
        return True
    except Exception as e:
        print(e)
        db.rollback()
        return False


async def view_users(roles):
    return db.query(User).filter(User.role.in_(roles)).all()


async def enable_user(db_user):
    db_user.enabled = True
    
    try:
        db.commit()
        return True
    
    except Exception as e:
        db.rollback()
        return False


async def disable_user(db_user):
    db_user.enabled = False
    
    try:
        db.commit()
        return True
    
    except Exception as e:
        db.rollback()
        return False