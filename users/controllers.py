from tokens import controllers as token_controllers
from users import models as user_models
from users import helpers as user_helpers


async def sign_in(user):
    ''' get user '''
    res = await user_models.get_user_by_phone(user.phone)
    if res:
        ''' check password '''
        if user_helpers.check_password(user.password, res.password):
            token_value = await token_controllers.refresh_token(res.id)
            return token_value
    return False


async def sign_out(token):
    await token_controllers.delete_token(token)


async def register_user(user):
    check = await user_models.get_user_by_phone(user.phone)
    print(check)
    if check == None:
        user.password = user_helpers.hash_password(user.password)
        
        #print(user, uid)
        res = await user_models.create_user(user)
        print(res)
        if res:
            return True
        else:
            return False
    return "exists"