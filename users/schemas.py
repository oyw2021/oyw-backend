from pydantic import BaseModel, Field, validator
from typing import Optional
import re

'''
    Phone Number is in string in both the schemas and models.
'''

class UserSignIn(BaseModel):
    phone: str
    password: str
    class Config:
        orm_mode = True

    @validator('phone')
    def phone_number_validate(cls, v):
        assert re.match(
            "^\d{10}$", v), 'Phone numbers must be 10 digit positive integers only'
        return v

    @validator('password')
    def password_length(cls, v):
        assert len(v) >= 8, 'Password must contain 8 characters or more'
        return v


class UpdateProfileSchema(BaseModel):
    phone: str = Field(...)
    name: str = Field(...)
    role: str = Field(...)
    class Config:
        orm_mode = True


class UpdatePasswordSchema(BaseModel):
    current_password: str = Field(...)
    new_password: str = Field(...)
    class Config:
        orm_mode = True

    @validator('current_password')
    def current_password_length(cls, v):
        assert len(v) >= 8, 'Password must contain 8 characters or more'
        return v

    @validator('new_password')
    def new_password_length(cls, v):
        assert len(v) >= 8, 'Password must contain 8 characters or more'
        return v


class DeleteUserSchema(BaseModel):
    phone: str = Field(...)
    password: str = Field(...)
    class Config:
        orm_mode = True
    
    @validator('phone')
    def phone_number_validate(cls, v):
        assert re.match(
            "^\d{10}$", v), 'Phone numbers must be 10 digit positive integers only'
        return v

    @validator('password')
    def password_length(cls, v):
        assert len(v) >= 8, 'Password must contain 8 characters or more'
        return v


class ResetPasswordSchema(BaseModel):
    phone: str = Field(...)
    password: str = Field(...)
    token: str = Field(...)
    class Config:
        orm_mode = True

    @validator('phone')
    def phone_number_validate(cls, v):
        assert re.match(
            "^\d{10}$", v), 'Phone numbers must be 10 digit positive integers only'
        return v

    @validator('password')
    def password_length(cls, v):
        assert len(v) >= 8, 'Password must contain 8 characters or more'
        return v


class UserSignUp(BaseModel):
    phone: str = Field(...)
    password: str = Field(...)
    name: str = Field(...)
    role: str = Field(...)

    class Config:
        orm_mode = True

    @validator('password')
    def password_valid(cls, v):
        assert len(v) >= 8, 'Password must contain 9 characters or more'
        return v


class UserSchema(BaseModel):
    phone: str = Field(...)
    
    class Config:
        orm_mode = True