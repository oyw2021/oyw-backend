from responses.standard_response_body import StandardResponseBody
from fastapi import APIRouter, Request, BackgroundTasks, Depends
from tokens import controllers as token_controllers
from users import controllers as user_controllers
from users import schemas as user_schemas
from pydantic import Field


router = APIRouter()

### Common Sign In for all
@router.post("/sign_in/", summary = "User authentication")
async def sign_in(user: user_schemas.UserSignIn):
    token = await user_controllers.sign_in(user)
    if token:
        # print(token.role)
        return StandardResponseBody(
                True, "Successfully logged in", token.token_value, {"user_role": token.role}
            )
    return StandardResponseBody(
        False, "Invalid username or password"
    )


@router.post("/sign_out/", summary = "User authentication")
async def sign_out(background_tasks: BackgroundTasks, token: dict = Depends(token_controllers.token_validation)):
    background_tasks.add_task(user_controllers.sign_out, token = token)
    return StandardResponseBody(
        True, "Successfully logged out"
    )