import urllib.request
import urllib.parse
 
def sendSMS(apikey, numbers, sender, message):
    data =  urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers,
        'message' : message, 'sender': sender})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    f = urllib.request.urlopen(request, data)
    fr = f.read()
    return(fr)
 
register_pass_sms_template = """Hi #name,
Your ticket(s) for Orlem Youth Week 2019 is successfully booked.

Please show this QR code #url while entering. All details regarding your ticket(s) are available on the link.

Regards,
OYW 2021"""


print(sendSMS(apikey="/7Uwc/6bVt4-yBlzqBRySKIp6HEeavgCYqvQ42vxln", 
        numbers='9769831280', sender='OYWOYA',
        message=register_pass_sms_template))


