import settings
import secrets


PASS_CODE_LENGTH = settings.PASS_CODE_LENGTH
PASS_SHORT_CODE_LENGTH = settings.PASS_SHORT_CODE_LENGTH


async def generate_codes():
    
    code = "OYW-" + secrets.token_hex(PASS_CODE_LENGTH)
    short_code = secrets.token_hex(PASS_SHORT_CODE_LENGTH)
    
    return code, short_code
