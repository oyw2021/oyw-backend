from responses.standard_response_body import StandardResponseBody
from fastapi import APIRouter, Request, BackgroundTasks
from passes import controllers as pass_controllers
from tokens.controllers import token_validation
from fastapi.param_functions import Depends
from passes import schemas as pass_schemas
from users.roles import RoleChecker
import settings


router = APIRouter()

check_user_can_register = RoleChecker(["RU", "SA", "RH"])
check_user_can_scan = RoleChecker(["SU", "SA", "SH", "RH"])

@router.options("/counts/")
@router.get("/counts/", summary = "Retrieves current total pass counts")
async def get_total_pass_counts():
    res = {
        "singing": settings.CURRENT_PASS_COUNT_SINGING,
        "drama": settings.CURRENT_PASS_COUNT_DRAMA,
        "dance": settings.CURRENT_PASS_COUNT_DANCE,
        "redeemed_singing": settings.REDEEMED_PASS_COUNT_SINGING,
        "redeemed_drama": settings.REDEEMED_PASS_COUNT_DRAMA,
        "redeemed_dance": settings.REDEEMED_PASS_COUNT_DANCE,
    }
    return StandardResponseBody(
        True, "Pass counts", data = res
    )


@router.post("/pass_details/", summary = "Retrieves customer pass details")
async def get_pass_details(codes: pass_schemas.ScanPass):
    # hello
    pass_details = await pass_controllers.get_pass_details(codes = codes)
    
    if pass_details == False:
        return StandardResponseBody(
            False, "Pass does not exist!"
        )
    return StandardResponseBody(
        True, "Pass details retrieved", data = pass_details
    )


@router.post("/register/", summary = "Register pass")
async def register_pass(background_tasks: BackgroundTasks, pass_details: pass_schemas.RegisterPass, token: dict = Depends(token_validation)):
    
    ### authorization
    if not check_user_can_register(token.role):
        return StandardResponseBody(False, "You are not authorized to register passes.", token = token.token_value)
    
    if pass_details.email == "":
        pass_details.email = None
    if pass_details.phone == "":
        pass_details.phone = None
    
    res = await pass_controllers.register_pass(pass_details = pass_details, bg = background_tasks)
    
    if res == True:
        return StandardResponseBody(
            True, "Pass has been registered", token = token.token_value
        )
    elif res == "exceeded":
        return StandardResponseBody(
            False, "Pass limit exceeded. Register new pass.", token = token.token_value
        )
    return StandardResponseBody(
        False, "Pass not registered", token = token.token_value
    )


@router.post("/scan/", summary = "Scan pass")
async def scan_pass(scan_details : pass_schemas.ScanPass, token: dict = Depends(token_validation)):
    if not check_user_can_scan(token.role):
        return StandardResponseBody(False, "You are not authorized to scan passes.", token = token.token_value)
    
    res = await pass_controllers.scan_pass(scan_details = scan_details)
    if res:
        return StandardResponseBody(
            True, "Scanned successfully", token = token.token_value, data = res
        )
    return StandardResponseBody(
        False, "Invalid pass", token = token.token_value
    )


@router.post("/accept/", summary = "Accept scanned pass")
async def accept_pass(background_tasks: BackgroundTasks, scan_details : pass_schemas.AcceptScanPass, token: dict = Depends(token_validation)):
    if not check_user_can_scan(token.role):
        return StandardResponseBody(False, "You are not authorized to accept passes.", token = token.token_value)
    
    res = await pass_controllers.accept_scanned_pass(scan_details = scan_details, background_tasks = background_tasks)
    if res == True:
        return StandardResponseBody(
            True, "Pass accepted", token = token.token_value
        )
    elif res == "balance":
        return StandardResponseBody(
            False, "Insufficient balance", token = token.token_value
        )
    return StandardResponseBody(
        False, "Scan failed", token = token.token_value
    )


@router.post("/view_all/", summary = "Retrieve all passes details")
async def view_all_passes(token: dict = Depends(token_validation)):
    
    ### authorization
    if not token.role:
        return StandardResponseBody(False, "You are not authorized to view pass details.", token = token.token_value)
    
    res = await pass_controllers.view_all_passes()
    
    if res:
        return StandardResponseBody(
            True, "Pass details retrieved", token = token.token_value, data = res
        )
    return StandardResponseBody(
        False, "Could not retrieve pass details", token = token.token_value
    )


@router.post("/send_pass_mail/", summary = "Sends pass mail to customer")
async def send_pass_mail(email: pass_schemas.EmailSchema):

    res = await pass_controllers.send_pass_mail(email.email)
    if res:
        return StandardResponseBody(
            True, "Pass sent to email"
        )
    return StandardResponseBody(
        False, "Could not send email, or email does not exist"
    )
