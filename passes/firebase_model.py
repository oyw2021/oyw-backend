from os import terminal_size
from db_setup import firebase_setup
import datetime
from firebase_admin.firestore import SERVER_TIMESTAMP



async def add_pass_transaction(pass_details, db_pass, amount):
    firebase_transaction = {
            'phone': db_pass.phone,
            'email': db_pass.email,
            'singing_count' : pass_details.singing_count,
            'drama_count' : pass_details.drama_count,
            'dance_count' : pass_details.dance_count,
            'amount': amount,
            'code': db_pass.code,
            'timestamp': datetime.datetime.now().__str__(),
        }
    ref = firebase_setup.ref.child('registration')
    ref.push(firebase_transaction)
    return True

async def scan_pass_transaction(db_pass, day, count):
    firebase_transaction = {
            "code": db_pass.code,
            'phone': db_pass.phone,
            'email': db_pass.email,
            'day': day,
            'count': count,            
            'amount': int(db_pass.amount),
            'timestamp': datetime.datetime.now().__str__(),
        }
    ref = firebase_setup.ref.child('scan')
    ref.push(firebase_transaction)
    return True


async def get_registration_transactions():
    ref = firebase_setup.ref.child('registration')
    transaction_values = ref.get()
    return transaction_values if transaction_values else {}


async def get_scan_transactions():
    ref = firebase_setup.ref.child('scan')
    transaction_values = ref.get()
    return transaction_values if transaction_values else {}


async def get_registration_transaction_by_id(r_id):
    reg = firebase_setup.ref.child('registration').child(r_id).get()
    return reg


async def get_scan_transaction_by_id(s_id):
    scan = firebase_setup.ref.child('scan').child(s_id).get()
    return scan


async def delete_registration_transaction(firebase_id : str):
    try:
        ref = firebase_setup.ref.child('registration').child(firebase_id).set({})
        # print(ref)
        return True
    except Exception as e:
        return False
    

async def delete_scan_transaction(firebase_id : str):
    try:
        ref = firebase_setup.ref.child('scan').child(firebase_id).set({})
        # print(ref)
        return True
    except Exception as e:
        return False