from tokens import controllers as token_controllers
from passes import helpers as pass_helpers
from passes import models as pass_models
from helpers import mail_helper
from helpers import sms_helper
import settings


async def register_pass(pass_details, bg):
    # print(pass_details)
    ### get pass object
    if pass_details.phone:
        db_pass = await pass_models.get_pass_by_phone(phone = pass_details.phone)
    elif pass_details.email:
        db_pass = await pass_models.get_pass_by_email(email = pass_details.email)
    
    if not db_pass:        
        ### generate unique codes
        codes = await pass_helpers.generate_codes()
        
        ### create pass for customer
        code = await pass_models.create_pass(pass_details = pass_details, codes = codes)
        if not code:
            return False
        
        db_pass = await pass_models.get_pass_by_code(code = code)
    
    ### check pass counts, return if exceeded limit
    print(db_pass.singing_count + pass_details.singing_count)
    if db_pass.singing_count + pass_details.singing_count > 4:
        return "exceeded"
    elif db_pass.drama_count + pass_details.drama_count > 4:
        return "exceeded"
    elif db_pass.dance_count + pass_details.dance_count > 4:
        return "exceeded"

    ### register pass
    x = await pass_models.register_pass(db_pass = db_pass, pass_details = pass_details)
    if x:
        ### send mail
        print(pass_details.email, settings.SEND_MAIL)
        if pass_details.email and settings.SEND_MAIL:
            print("sending mail")
            await mail_helper.send_register_mail(background_tasks = bg, db_pass = db_pass)

        if pass_details.phone and settings.SEND_SMS:
            print("Sending SMS")
            #await sms_helper.send_register_pass_sms(background_tasks = bg, db_pass = db_pass)
        
        ### update pass counts
        bg.add_task(update_total_pass_counts)
        return True
        
    return False


async def scan_pass(scan_details):
    ### code / short code, get pass
    db_pass = None
    if scan_details.code != "":
        db_pass = await pass_models.get_pass_by_code(code = scan_details.code)
    elif scan_details.short_code != "":
        db_pass = await pass_models.get_pass_by_short_code(short_code = scan_details.short_code)
    
    if db_pass == None:     
        return False
    
    if scan_details.day == 1:
        db_pass.balance = db_pass.singing_count - db_pass.singing_redeemed_count
    elif scan_details.day == 2:
        db_pass.balance = db_pass.drama_count - db_pass.drama_redeemed_count
    elif scan_details.day == 3:
        db_pass.balance = db_pass.dance_count - db_pass.dance_redeemed_count

    return db_pass


async def accept_scanned_pass(scan_details, background_tasks):
    ### code / short code, get pass
    if scan_details.code not in ["", None]:
        db_pass = await pass_models.get_pass_by_code(code = scan_details.code)
    elif scan_details.short_code not in ["", None]:
        db_pass = await pass_models.get_pass_by_short_code(short_code = scan_details.short_code)
    if db_pass == None:     
        return False
    
    ### check redeemed vs booked
    if scan_details.day == 1:
        if db_pass.singing_redeemed_count + scan_details.count > db_pass.singing_count:
            return "balance"
        db_pass.singing_redeemed_count += scan_details.count
    elif scan_details.day == 2:
        if db_pass.drama_redeemed_count + scan_details.count > db_pass.drama_count:
            return "balance"
        db_pass.drama_redeemed_count += scan_details.count
    elif scan_details.day == 3:
        if db_pass.dance_redeemed_count + scan_details.count > db_pass.dance_count:
            return "balance"
        db_pass.dance_redeemed_count += scan_details.count
        
    db_pass = await pass_models.accept_scanned_pass(db_pass = db_pass, day = scan_details.day, count = scan_details.count)
    print(db_pass)
    if db_pass:
        ### send mail
        if db_pass.email and settings.SEND_MAIL:
            print("sending accept mail")
            await mail_helper.send_scan_mail(background_tasks = background_tasks, db_pass = db_pass)
            pass

        if db_pass.phone and settings.SEND_SMS:
            print("Sending SMS")
            #await sms_helper.send_scan_pass_sms(background_tasks = background_tasks, db_pass = db_pass)
            return True
        
        ### update pass counts
        background_tasks.add_task(update_total_pass_counts)
        return True
    
    return False


async def get_pass_details(codes):    
    ### code / short code, get pass
    if codes.code != "":
        pass_details = await pass_models.get_pass_by_code(code = codes.code)
    elif codes.short_code != "":
        pass_details = await pass_models.get_pass_by_short_code(short_code = codes.short_code)

    if not pass_details:
        return False
    else:
        pass_details = pass_details.__dict__
        pass_details.pop("amount")
        pass_details["pass_number"] = pass_details.pop("id")
        return pass_details


async def view_all_passes():
    res = await pass_models.get_all_passes()
    x = []
    if res:
        for p in res:
            p = p.__dict__
            p["pass_number"] = p.pop("id")
            x.append(p)
        return x
    return False


async def update_total_pass_counts():
    counts = await pass_models.get_total_pass_counts()
    # print(counts)
    if counts:
        settings.CURRENT_PASS_COUNT_SINGING, settings.CURRENT_PASS_COUNT_DRAMA, settings.CURRENT_PASS_COUNT_DANCE, settings.REDEEMED_PASS_COUNT_SINGING, settings.REDEEMED_PASS_COUNT_DRAMA, settings.REDEEMED_PASS_COUNT_DANCE = counts
        return True
    return False


async def undo_pass(code : str):
    resp = pass_models.get_pass_by_code(code)
    print(resp)


async def undo_pass_register(db_pass, pass_details):
    ### if pass balance > undo balance    
    if (db_pass.singing_count - db_pass.singing_redeemed_count >= pass_details["singing_count"]):
        if (db_pass.drama_count - db_pass.drama_redeemed_count >= pass_details["drama_count"]):
            if (db_pass.dance_count - db_pass.dance_redeemed_count >= pass_details["dance_count"]):
                db_pass.singing_count -= pass_details["singing_count"]
                db_pass.drama_count -= pass_details["drama_count"]
                db_pass.dance_count -= pass_details["dance_count"]
                db_pass.amount -= pass_details["amount"]

                res = await pass_models.update_pass(db_pass)
                return res
    await update_total_pass_counts()
    return "balance"


async def undo_pass_scan(db_pass, pass_details):
    ### if pass balance > undo balance
    if pass_details["day"] == 1:
        if db_pass.singing_redeemed_count < pass_details["count"]:
            return "balance"
        db_pass.singing_redeemed_count -= pass_details["count"]
            
    elif pass_details["day"] == 2:
        if db_pass.drama_redeemed_count < pass_details["count"]:
            return "balance"
        db_pass.drama_redeemed_count -= pass_details["count"]
            
    elif pass_details["day"] == 3:
        if db_pass.dance_redeemed_count < pass_details["count"]:
            return "balance"
        db_pass.dance_redeemed_count -= pass_details["count"]
    
    db_pass.amount -= pass_details["amount"]

    res = await pass_models.update_pass(db_pass)
    await update_total_pass_counts()
    return res


async def send_pass_mail(email):
    db_pass = await pass_models.get_pass_by_email(email)
    if db_pass:
        x = await mail_helper.send_pass_details_mail(db_pass)
        return x
    return False