from pydantic import BaseModel, Field, validator
from typing import Optional
import settings
import re


class RegisterPass(BaseModel):
    phone : Optional[str] = None
    email : Optional[str] = None
    name : Optional[str] = None
    
    singing_count : int = Field(...)
    drama_count : int = Field(...)
    dance_count : int = Field(...)

    class Config:
        orm_mode = True
    
    @validator('singing_count')
    def check_singing_count(cls, v):
        assert v < 5, 'Pass booking limit is 4'
        return v
    
    @validator('drama_count')
    def check_drama_count(cls, v):
        assert v < 5, 'Pass booking limit is 4'
        return v
    
    @validator('dance_count')
    def check_dance_count(cls, v):
        assert v < 5, 'Pass booking limit is 4'
        return v


class ScanPass(BaseModel):
    code : Optional[str] = ""
    short_code : Optional[str] = ""
    day : int = Field(...)

    class Config:
        orm_mode = True


class AcceptScanPass(BaseModel):
    code : Optional[str] = ""
    short_code : Optional[str] = ""
    day : int = Field(...)
    count : int = Field(...)

    class Config:
        orm_mode = True
    
    @validator('count')
    def check_count(cls, v):
        assert v < 5, 'Pass scanning limit is 4'
        return v
    
    
class EmailSchema(BaseModel):
    email : str

    class Config:
        orm_mode = True