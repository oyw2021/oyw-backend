from sqlalchemy import Column, Integer, String, ForeignKey, Numeric, BigInteger
from db_setup.mysql_setup import Base, SessionLocal
from sqlalchemy import func
import settings
import datetime
from passes import firebase_model


db = SessionLocal()


class Passes(Base):

    __tablename__ = "passes"

    id = Column(BigInteger, primary_key=True, index=True)
    phone = Column(String(10), unique=True)
    email = Column(String(100), unique=True)
    name = Column(String(30))

    singing_count = Column(Integer, default=0)
    drama_count = Column(Integer, default=0)
    dance_count = Column(Integer, default=0)

    singing_redeemed_count = Column(Integer, default=0)
    drama_redeemed_count = Column(Integer, default=0)
    dance_redeemed_count = Column(Integer, default=0)

    amount = Column(Numeric, default=0)

    code = Column(String(100), unique=True)
    short_code = Column(String(20), unique=True)


async def get_pass_by_code(code):
    return db.query(Passes).filter(Passes.code == code).first()


async def get_pass_by_short_code(short_code):
    return db.query(Passes).filter(Passes.short_code == short_code).first()


async def get_pass_by_phone(phone):
    return db.query(Passes).filter(Passes.phone == phone).first()


async def get_pass_by_email(email):
    return db.query(Passes).filter(Passes.email == email).first()


async def create_pass(pass_details, codes):
    db_pass = Passes(
        phone=pass_details.phone,
        email=pass_details.email,
        name=pass_details.name,

        code=codes[0],
        short_code=codes[1]
    )

    try:
        db.add(db_pass)
        db.commit()
        return db_pass.code

    except Exception as e:
        print(e)
        db.rollback()
        return False


async def register_pass(db_pass, pass_details):
    # print(db_pass, _pass)
    amount = pass_details.singing_count * settings.SINGING_PASS_COST + pass_details.drama_count * \
        settings.DRAMA_PASS_COST + pass_details.dance_count * settings.DANCE_PASS_COST

    try:
        db_pass.singing_count += pass_details.singing_count
        db_pass.drama_count += pass_details.drama_count
        db_pass.dance_count += pass_details.dance_count
        db_pass.amount += amount
        
        x = await firebase_model.add_pass_transaction(pass_details, db_pass,amount)
        print(x)
        
        db.commit()
        print("registered")

        return True

    except Exception as e:
        print(e)
        db.rollback()
        return False


async def get_pass_details(link, day):
    details = db.query(Passes).filter(
        Passes.link == link
    ).first()

    return details


async def accept_scanned_pass(db_pass, day, count):
    try:        
        await firebase_model.scan_pass_transaction(db_pass, day, count)
        db.commit()
        
        return db_pass

    except Exception as e:
        db.rollback()
        return False


async def get_total_pass_counts():
    passes = db.query(Passes)
    try:
        return db.query(Passes).with_entities(
            func.sum(Passes.singing_count),
            func.sum(Passes.drama_count),
            func.sum(Passes.dance_count),
            func.sum(Passes.singing_redeemed_count),
            func.sum(Passes.drama_redeemed_count),
            func.sum(Passes.dance_redeemed_count),
        ).first()

    except Exception as e:
        print(e)
        return False


async def test_counts():
    return db.query(Passes).with_entities(
        func.sum(Passes.singing_count),
        func.sum(Passes.drama_count),
        func.sum(Passes.dance_count),
        func.sum(Passes.singing_redeemed_count),
        func.sum(Passes.drama_redeemed_count),
        func.sum(Passes.dance_redeemed_count),
    ).first()


async def update_pass(db_pass):
    try:        
        db.commit()        
        return True

    except Exception as e:
        db.rollback()
        return False


async def get_all_passes():
    res = db.query(Passes).all()
    print(res[0])
    return res