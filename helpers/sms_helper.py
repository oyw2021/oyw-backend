from twilio.rest import Client
from fastapi import BackgroundTasks
import env


client = Client(env.SMS_SID, env.SMS_AUTH_TOKEN) 


async def send_register_pass_sms(background_tasks: BackgroundTasks, db_pass):
    
    link = f"{env.PASS_LINK}{db_pass.code}"
    
    name = db_pass.name
    if name == "":
        name = "there"
    
    body = f"""
Hi {name},
Your ticket(s) for Orlem Youth Week 2021 have been booked successfully!

All details regarding your ticket(s) are available on the link {link}.

Please show the QR code while entering, or use the code: { db_pass.short_code }

Regards,
The OYW Team.
"""
    message = client.messages.create(  
        messaging_service_sid = env.SMS_MID, 
        body = body,      
        to = f"+91{ db_pass.phone }"
    ) 
    
    return message.sid


async def send_scan_pass_sms(background_tasks: BackgroundTasks, db_pass):
    
    link = f"{env.PASS_LINK}{db_pass.code}"
    
    singing = db_pass.singing_count - db_pass.singing_redeemed_count
    drama = db_pass.drama_count - db_pass.drama_redeemed_count
    dance = db_pass.dance_count - db_pass.dance_redeemed_count
    
    name = db_pass.name
    if name == "":
        name = "there"
    
    body = f"""
Hi {name},
Your ticket(s) for Orlem Youth Week 2021 have been redeemed!

Balance:
- Singing: { singing }
- Drama: { drama }
- Dance: { dance }

All details regarding your ticket(s) are available on the link {link}.

Please show the QR code while entering, or use the code: { db_pass.short_code }

Regards,
The OYW Team.
"""
    message = client.messages.create(  
        messaging_service_sid = env.SMS_MID, 
        body = body,      
        to = f"+91{ db_pass.phone }"
    ) 
    
    return message.sid