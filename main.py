from fastapi.responses import RedirectResponse, HTMLResponse
from starlette.middleware.cors import CORSMiddleware
from passes import controllers as pass_controllers
from db_setup.mysql_setup import Base, engine
from fastapi import BackgroundTasks
from fastapi import FastAPI
from helpers import mail_helper
import datetime
import settings
import uvicorn
import pytz
import env

from users.routes import router as user_router
from tokens.routes import router as token_router
from passes.routes import router as pass_router
from admins.routes import router as admin_router


Base.metadata.create_all(engine)

startup_timestamp = False

app = FastAPI(
    title="OYW APIs 2021",
    description=f"Backend APIs for OYW E-Ticketing."
)

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://127.0.0.1",
    "http://127.0.0.1:3000",
    "http://orlemyouthweek.site",
    "https://orlemyouthweek.site",
    "http://www.orlemyouthweek.site",
    "https://www.orlemyouthweek.site",
    "http://dev.orlemyouthweek.site",
    "https://tickets.orlemyouthweek.site",
    "http://tickets.orlemyouthweek.site",
    "https://live.orlemyouthweek.site",    
    "http://live.orlemyouthweek.site",
    "https://oyw-frontend.herokuapp.com",
    "http://oyw-frontend.herokuapp.com",
    "http://45.79.127.39",
    "http://172.104.206.245"
]


@app.get("/", include_in_schema = False)
async def home_to_doc():
    return RedirectResponse("/docs")


@app.get("/last_updated")
async def last_updated():
    return {"last_updated": settings.updated_at}


# @app.options("/")
# async def options_route():
#     return True


@app.on_event("startup")
async def startup_get_count():
    print("Starting app...")
    settings.updated_at = datetime.datetime.now(tz = pytz.timezone("Asia/Kolkata"))
    await pass_controllers.update_total_pass_counts()
    # await mail_helper.send_pass_mail(BackgroundTasks)


app.include_router(user_router, tags=['User Auth'], prefix="/auth")
app.include_router(token_router, tags=["Tokens"], prefix="/tokens")
app.include_router(pass_router, tags=["Passes"], prefix="/passes")
app.include_router(admin_router, tags=["Admin"], prefix="/admin")


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "OPTIONS"],
    allow_headers=["*"]
)


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host = "127.0.0.1",
        port = 8000,
        debug = True
    )